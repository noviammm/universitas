package com.noviam.restapi.entities;

import javax.persistence.*;

@Entity
@Table(name = "tbl_matakuliah")
@Inheritance(strategy = InheritanceType.JOINED)

public class Matakuliah {
	
	@Id
	private String id_matkul;
	
	@Column(nullable = false)
	private String nama_matkul;
	
	@Column(nullable = false)
	private int sks_matkul;
	
	public Matakuliah(){
		super();
	}
	
	public Matakuliah(String id_matkul, String nama_matkul, int sks_matkul){
		super();
		this.id_matkul = id_matkul;
		this.nama_matkul = nama_matkul;
		this.sks_matkul = sks_matkul;
		}

	public String getId_matkul() {
		return id_matkul;
	}

	public void setId_matkul(String id_matkul) {
		this.id_matkul = id_matkul;
	}

	public String getNama_matkul() {
		return nama_matkul;
	}

	public void setNama_matkul(String nama_matkul) {
		this.nama_matkul = nama_matkul;
	}

	public int getSks_matkul() {
		return sks_matkul;
	}

	public void setSks_matkul(int sks_matkul) {
		this.sks_matkul = sks_matkul;
	}
}
