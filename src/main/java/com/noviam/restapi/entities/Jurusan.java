package com.noviam.restapi.entities;
import javax.persistence.*;

@Entity
@Table(name="tbl_jurusan")
@Inheritance(strategy=InheritanceType.JOINED)
public class Jurusan {
	@Id
	private String id_jurusan;
	
	@Column(nullable=false)
	private String id_fakultas;
	
	@Column(nullable=false)
	private String nama_jurusan;
	
	public Jurusan() {
		super();
	}
	
	public Jurusan(
			String id_jurusan,
			String id_fakultas,
			String nama_jurusan
	){
		super();
		this.id_jurusan=id_jurusan;
		this.id_fakultas=id_fakultas;
		this.nama_jurusan=nama_jurusan;
	}
	
	
	public String getIdJurusan() {
		return id_jurusan;
	}
	
	public void setIdJurusan(String id_jurusan) {
		this.id_jurusan=id_jurusan;
	}
	
	public String getIdFakultas() {
		return id_jurusan;
	}
	
	public void setIdFakultas(String id_fakultas) {
		this.id_fakultas=id_fakultas;
	}
	
	
	public String getNamaJurusan() {
		return nama_jurusan;
	}
	
	public void setNamaJurusan(String nama_jurusan) {
		this.nama_jurusan=nama_jurusan;
	}
	
	
	
	
}
