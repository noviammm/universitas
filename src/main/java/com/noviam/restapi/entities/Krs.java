package com.noviam.restapi.entities;

import javax.persistence.*;


@Entity
@Table(name = "tbl_krs")
@Inheritance(strategy = InheritanceType.JOINED)
public class Krs {
	
	@Column(nullable = false)
	private String no;
	
	@Id
	private String id_krs;
	
	@Column(nullable = false)
	private String npm_mhs;
	
	@Column(nullable = false)
	private String id_jurusan;
	
	@Column(nullable = false)
	private String id_matkul;
	
	@Column(nullable = false)
	private String dospem;
	
	@Column(nullable = false)
	private int semester;
	
	@Column(nullable = false)
	private String tahun_ajaran;
	
	public Krs() {
		super();
	}
	
	public Krs(String no, String id_krs, String npm_mhs,
			String id_jurusan, String id_matkul, String dospem,
			int semester, String tahun_ajaran) {
		super();
		this.no = no;
		this.id_krs = id_krs;
		this.npm_mhs = npm_mhs;
		this.id_jurusan = id_jurusan;
		this.id_matkul = id_matkul;
		this.dospem = dospem;
		this.semester = semester;
		this.tahun_ajaran = tahun_ajaran;
		
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getId_krs() {
		return id_krs;
	}

	public void setId_krs(String id_krs) {
		this.id_krs = id_krs;
	}

	public String getNpm_mhs() {
		return npm_mhs;
	}

	public void setNpm_mhs(String npm_mhs) {
		this.npm_mhs = npm_mhs;
	}

	public String getId_jurusan() {
		return id_jurusan;
	}

	public void setId_jurusan(String id_jurusan) {
		this.id_jurusan = id_jurusan;
	}

	public String getId_matkul() {
		return id_matkul;
	}

	public void setId_matkul(String id_matkul) {
		this.id_matkul = id_matkul;
	}

	public String getDospem() {
		return dospem;
	}

	public void setDospem(String dospem) {
		this.dospem = dospem;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

	public String getTahun_ajaran() {
		return tahun_ajaran;
	}

	public void setTahun_ajaran(String tahun_ajaran) {
		this.tahun_ajaran = tahun_ajaran;
	}
	
}