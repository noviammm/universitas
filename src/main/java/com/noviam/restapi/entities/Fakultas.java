package com.noviam.restapi.entities;


import javax.persistence.*;

@Entity
@Table(name = "tbl_fakultas")
@Inheritance(strategy = InheritanceType.JOINED)
public class Fakultas {
	@Id
	private String id_fakultas;
	
	@Column(nullable = false)
	private String nama_fakultas;
	
	public Fakultas() {
		super();
	}
	
	public Fakultas(String id_fakultas, String nama_fakultas) {
		super();
		this.id_fakultas = id_fakultas;
		this.nama_fakultas = nama_fakultas;
	}

	public String getId_fakultas() {
		return id_fakultas;
	}

	public void setId_fakultas(String id_fakultas) {
		this.id_fakultas = id_fakultas;
	}

	public String getNama_fakultas() {
		return nama_fakultas;
	}

	public void setNama_fakultas(String nama_fakultas) {
		this.nama_fakultas = nama_fakultas;
	}
	
}
