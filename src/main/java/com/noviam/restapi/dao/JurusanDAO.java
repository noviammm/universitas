package com.noviam.restapi.dao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.noviam.restapi.entities.Jurusan;

@Repository
public class JurusanDAO implements IJurusanDAO{
	@PersistenceContext
	EntityManager em;
	
	@Override
	@Transactional
	public Jurusan getJurusan(String id_jurusan) {
		return em.find(Jurusan.class, id_jurusan);
	}
	
	@Override
	@Transactional
	public List<Jurusan> getJurusans(){
		List<Jurusan> resultList = em.createQuery("from Jurusan", Jurusan.class).getResultList();
		return resultList;
	}
	
	@Override
	@Transactional
	public void createJurusan(Jurusan jurusan) {
		em.persist(jurusan);
	}
	
	@Override
	@Transactional
	public void updateJurusan(Jurusan jurusan) {
		em.merge(jurusan);
	}
	
	@Override
	@Transactional
	public void deleteJurusan(String id_jurusan) {
		Jurusan jurusan = this.getJurusan(id_jurusan);
		em.remove(jurusan);
	}
	
	@Override
	@Transactional
	public List<Jurusan> getJurusanSort(){
		List<Jurusan> result = em.createNativeQuery("select * from tbl_jurusan ORDER BY id_fakultas DESC", Jurusan.class).getResultList();
		return result;
	}
	
	
}
