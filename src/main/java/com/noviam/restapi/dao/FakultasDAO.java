package com.noviam.restapi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.noviam.restapi.entities.Fakultas;

@Repository
public class FakultasDAO implements IFakultasDAO {
	@PersistenceContext
	EntityManager em;
	
	@Override
	@Transactional
	public Fakultas getFakultas(String id_fakultas ) {
		return em.find(Fakultas.class, id_fakultas);
	}

	@Override
	@Transactional
	public List<Fakultas> getFakultases() {
		List<Fakultas> resultList = em.createQuery("FROM Fakultas", Fakultas.class).getResultList();
		return resultList;
	}
	
	@Override
	@Transactional
	public void createFakultas(Fakultas fakultas) {
		em.persist(fakultas);	
	}
	
	@Override
	@Transactional
	public void updateFakultas(Fakultas fakultas) {
		em.merge(fakultas);
	}
	
	@Override
	@Transactional
	public void deleteFakultas(String id_fakultas) {
		Fakultas fakultas = this.getFakultas(id_fakultas);
		em.remove(fakultas);
	}
	
	
	
}
