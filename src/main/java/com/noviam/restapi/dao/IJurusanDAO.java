package com.noviam.restapi.dao;
import java.util.List;
import com.noviam.restapi.entities.Jurusan;

public interface IJurusanDAO {
	public List<Jurusan> getJurusans();
	public List<Jurusan> getJurusanSort();
	public Jurusan getJurusan(String id_jurusan);
	public void createJurusan(Jurusan jurusan);
	public void updateJurusan(Jurusan jurusan);
	public void deleteJurusan(String id_jurusan);
}
