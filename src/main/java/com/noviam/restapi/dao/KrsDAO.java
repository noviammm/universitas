package com.noviam.restapi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.noviam.restapi.entities.Krs;


@Repository
public class KrsDAO implements IKrsDAO {
	@PersistenceContext
	EntityManager em;
	
	@Override
	@Transactional
	public Krs getKrs(String id_krs ) {
		return em.find(Krs.class, id_krs);
	}

	@Override
	@Transactional
	public List<Krs> getKrses() {
		List<Krs> resultList = em.createQuery("FROM Krs", Krs.class).getResultList();
		return resultList;
	}
	
	@Override
	@Transactional
	public void createKrs(Krs krs) {
		em.persist(krs);	
	}
	
	@Override
	@Transactional
	public void updateKrs(Krs krs) {
		em.merge(krs);
	}
	
	@Override
	@Transactional
	public void deleteKrs(String id_krs) {
		Krs krs = this.getKrs(id_krs);
		em.remove(krs);
	}

	@Override
	@Transactional
	public List<Krs> getNamaDanMatkul() {
		List<Krs> result = em.createNativeQuery("SELECT tbl_matakuliah.nama_matkul as nama_matkul, tbl_mahasiswa.nama_mhs as nama_mhs"
				+ "FROM tbl_krs "
				+ "LEFT JOIN tbl_matakuliah ON tbl_matakuliah.id_matkul = tbl_krs.id_matkul "
				+ "LEFT JOIN tbl_mahasiswa ON tbl_mahasiswa.npm_mhs = tbl_krs.npm_mhs"
				+ " ORDER BY nama_mhs", Krs.class).getResultList();
		return result;
	}
}