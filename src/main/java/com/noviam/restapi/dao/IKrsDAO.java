package com.noviam.restapi.dao;

import java.util.List;
import com.noviam.restapi.entities.Krs;

public interface IKrsDAO {
	public List<Krs> getKrses();
	
	public List<Krs> getNamaDanMatkul();

	public Krs getKrs(String id_krs);
	
	public void createKrs(Krs krs);
	
	public void updateKrs(Krs krs);
	
	public void deleteKrs(String id_krs);

}
