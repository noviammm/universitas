package com.noviam.restapi.dao;

import java.util.List;

import com.noviam.restapi.entities.Mahasiswa;

public interface IMahasiswaDAO {
	public List<Mahasiswa> getMahasiswas();

	public Mahasiswa getMahasiswa(String npm_mhs);
	
	public void createMahasiswa(Mahasiswa mahasiswa);
	
	public void updateMahasiswa(Mahasiswa mahasiswa);
	
	public void deleteMahasiswa(String npm_mhs);

}
