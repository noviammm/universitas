package com.noviam.restapi.dao;
import java.util.List;
import com.noviam.restapi.entities.Fakultas;

public interface IFakultasDAO {
	public List<Fakultas> getFakultases();

	public Fakultas getFakultas(String id_fakultas);
	
	public void createFakultas(Fakultas fakultas);
	
	public void updateFakultas(Fakultas fakultas);
	
	public void deleteFakultas(String id_fakultas);

}
