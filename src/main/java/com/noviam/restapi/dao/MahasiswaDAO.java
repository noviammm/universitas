package com.noviam.restapi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.noviam.restapi.entities.Mahasiswa;

@Repository
public class MahasiswaDAO implements IMahasiswaDAO {
	@PersistenceContext
	EntityManager em;
	
	@Override
	@Transactional
	public Mahasiswa getMahasiswa(String npm_mhs ) {
		return em.find(Mahasiswa.class, npm_mhs);
	}

	@Override
	@Transactional
	public List<Mahasiswa> getMahasiswas() {
		List<Mahasiswa> resultList = em.createQuery("FROM Mahasiswa", Mahasiswa.class).getResultList();
		return resultList;
	}
	
	@Override
	@Transactional
	public void createMahasiswa(Mahasiswa mahasiswa) {
		em.persist(mahasiswa);	
	}
	
	@Override
	@Transactional
	public void updateMahasiswa(Mahasiswa mahasiswa) {
		em.merge(mahasiswa);
	}
	
	@Override
	@Transactional
	public void deleteMahasiswa(String npm_mhs) {
		Mahasiswa mahasiswa = this.getMahasiswa(npm_mhs);
		em.remove(mahasiswa);
	}
	
	
	
}