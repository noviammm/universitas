package com.noviam.restapi.dao;

import java.util.List;

import com.noviam.restapi.entities.Matakuliah;

public interface IMatakuliahDAO {
	public List<Matakuliah> getMatkuls();
	
	public Matakuliah getMatkul(String id_matkul);
	
	public void createMatkul(Matakuliah matkul);
	
	public void updateMatkul(Matakuliah matkul);
	
	public void deleteMatkul(String id_matkul);

}
