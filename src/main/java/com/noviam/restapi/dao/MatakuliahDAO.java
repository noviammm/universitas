package com.noviam.restapi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.noviam.restapi.entities.Matakuliah;

@Repository
public class MatakuliahDAO implements IMatakuliahDAO{
	@PersistenceContext
	EntityManager em;
	
	@Override
	@Transactional
	public List<Matakuliah> getMatkuls() {
		List<Matakuliah> resultList = em.createQuery("FROM Matakuliah", Matakuliah.class).getResultList();
		return resultList;
	}

	@Override
	public Matakuliah getMatkul(String id_matkul) {
		return em.find(Matakuliah.class, id_matkul);
	}

	@Override
	public void createMatkul(Matakuliah matkul) {
		em.persist(matkul);
	}

	@Override
	public void updateMatkul(Matakuliah matkul) {
		em.merge(matkul);
	}

	@Override
	public void deleteMatkul(String id_matkul) {
		Matakuliah matkul = this.getMatkul(id_matkul);
		em.remove(matkul);
	}
	

}
