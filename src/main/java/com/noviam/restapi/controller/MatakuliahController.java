package com.noviam.restapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.noviam.restapi.dto.FakultasDTO;
import com.noviam.restapi.dto.MatakuliahDTO;
import com.noviam.restapi.exceptions.InternalServerError;
import com.noviam.restapi.exceptions.WrongParameters;
import com.noviam.restapi.service.MatakuliahService;

@RestController
@RequestMapping("/api/matakuliah")
@CrossOrigin

public class MatakuliahController {
	@Autowired
	MatakuliahService service;
	
	@RequestMapping(value="", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<List<MatakuliahDTO>> readAll() {
		List<MatakuliahDTO> matkul = service.getAllMatkuls();
		return new ResponseEntity<>(matkul, HttpStatus.OK);
	}
	
	@RequestMapping(value="{id_matkul}", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<MatakuliahDTO> read(@PathVariable String id_matkul) {
		MatakuliahDTO matkul = service.getMatkulById(id_matkul);
		return new ResponseEntity<>(matkul, HttpStatus.OK);
	}
	
	@RequestMapping(value="", method=RequestMethod.POST, produces="application/json" )
	public ResponseEntity<FakultasDTO> create(@RequestBody MatakuliahDTO matkul) {
		try {
			service.createMatkul(matkul);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="", method=RequestMethod.PUT, produces="application/json" )
	public ResponseEntity<MatakuliahDTO> update(@RequestBody MatakuliahDTO matkul) {
		try {
			service.updateMatkul(matkul);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="{id_matkul}", method=RequestMethod.DELETE, produces="application/json" )
	public ResponseEntity<MatakuliahDTO> delete(@PathVariable String id_matkul) {
		try {
			service.deleteMatkul(id_matkul);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
}
