package com.noviam.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.noviam.restapi.dto.JurusanDTO;
import com.noviam.restapi.exceptions.InternalServerError;
import com.noviam.restapi.exceptions.WrongParameters;
import com.noviam.restapi.service.JurusanService;

import java.util.List;

@RestController
@RequestMapping("/api/jurusan")
@CrossOrigin
public class JurusanController {
	@Autowired
	JurusanService service;

	@RequestMapping(value="", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<List<JurusanDTO>> readAll() {
		List<JurusanDTO> jurusan = service.getAllJurusans();
		return new ResponseEntity<>(jurusan, HttpStatus.OK);
	}
	
	@RequestMapping(value="/desc",method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<List<JurusanDTO>> readDesc(){
		List<JurusanDTO> jurusan = service.getJurusanDesc();
		return new ResponseEntity<>(jurusan, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="{id_jurusan}", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<JurusanDTO> read(@PathVariable String id_jurusan) {
		JurusanDTO jurusan = service.getJurusanByID(id_jurusan);
		return new ResponseEntity<>(jurusan, HttpStatus.OK);
	}

	@RequestMapping(value="", method=RequestMethod.POST, produces="application/json" )
	public ResponseEntity<JurusanDTO> create(@RequestBody JurusanDTO jurusan) {
		try {
			service.createJurusan(jurusan);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="", method=RequestMethod.PUT, produces="application/json" )
	public ResponseEntity<JurusanDTO> update(@RequestBody JurusanDTO jurusan) {
		try {
			service.updateJurusan(jurusan);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="{id_jurusan}", method=RequestMethod.DELETE, produces="application/json" )
	public ResponseEntity<JurusanDTO> delete(@PathVariable String id_jurusan) {
		try {
			service.deleteJurusan(id_jurusan);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
}
