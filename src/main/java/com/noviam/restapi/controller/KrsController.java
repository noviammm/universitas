package com.noviam.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.noviam.restapi.dto.KrsDTO;
import com.noviam.restapi.exceptions.InternalServerError;
import com.noviam.restapi.exceptions.WrongParameters;
import com.noviam.restapi.service.KrsService;

import java.util.List;

@RestController
@RequestMapping("/api/krs")
@CrossOrigin
public class KrsController {
	@Autowired
	KrsService service;
	
	@RequestMapping(value="", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<List<KrsDTO>> readAll() {
		List<KrsDTO> krs = service.getAllKrses();
		return new ResponseEntity<>(krs, HttpStatus.OK);
	}
	
	@RequestMapping(value="/nama_matkul", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<List<KrsDTO>> readNamaDanMatkul() {
		List<KrsDTO> krs = service.getNamaDanMatkul();
		return new ResponseEntity<>(krs, HttpStatus.OK);
	}
	
	@RequestMapping(value="{id_krs}", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<KrsDTO> read(@PathVariable String id_krs) {
		KrsDTO krs = service.getKrsById(id_krs);
		return new ResponseEntity<>(krs, HttpStatus.OK);
	}
	
	@RequestMapping(value="", method=RequestMethod.POST, produces="application/json" )
	public ResponseEntity<KrsDTO> create(@RequestBody KrsDTO krs) {
		try {
			service.createKrs(krs);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="", method=RequestMethod.PUT, produces="application/json" )
	public ResponseEntity<KrsDTO> update(@RequestBody KrsDTO krs) {
		try {
			service.updateKrs(krs);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="{id_krs}", method=RequestMethod.DELETE, produces="application/json" )
	public ResponseEntity<KrsDTO> delete(@PathVariable String id_krs) {
		try {
			service.deleteKrs(id_krs);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	
	
	
	
}