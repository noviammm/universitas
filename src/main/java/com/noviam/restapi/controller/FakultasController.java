package com.noviam.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.noviam.restapi.dto.FakultasDTO;
import com.noviam.restapi.exceptions.InternalServerError;
import com.noviam.restapi.exceptions.WrongParameters;
import com.noviam.restapi.service.FakultasService;

import java.util.List;

@RestController
@RequestMapping("/api/fakultas")
@CrossOrigin
public class FakultasController {
	@Autowired
	FakultasService service;
	
	@RequestMapping(value="", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<List<FakultasDTO>> readAll() {
		List<FakultasDTO> fakultas = service.getAllFakultases();
		return new ResponseEntity<>(fakultas, HttpStatus.OK);
	}
	
	@RequestMapping(value="{id_fakultas}", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<FakultasDTO> read(@PathVariable String id_fakultas) {
		FakultasDTO fakultas = service.getFakultasById(id_fakultas);
		return new ResponseEntity<>(fakultas, HttpStatus.OK);
	}
	
	@RequestMapping(value="", method=RequestMethod.POST, produces="application/json" )
	public ResponseEntity<FakultasDTO> create(@RequestBody FakultasDTO fakultas) {
		try {
			service.createFakultas(fakultas);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="", method=RequestMethod.PUT, produces="application/json" )
	public ResponseEntity<FakultasDTO> update(@RequestBody FakultasDTO fakultas) {
		try {
			service.updateFakultas(fakultas);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="{id_fakultas}", method=RequestMethod.DELETE, produces="application/json" )
	public ResponseEntity<FakultasDTO> delete(@PathVariable String id_fakultas) {
		try {
			service.deleteFakultas(id_fakultas);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	
	
	
	
}
