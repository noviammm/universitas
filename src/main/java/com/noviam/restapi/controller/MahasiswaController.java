package com.noviam.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.noviam.restapi.dto.MahasiswaDTO;
import com.noviam.restapi.exceptions.InternalServerError;
import com.noviam.restapi.exceptions.WrongParameters;
import com.noviam.restapi.service.MahasiswaService;

import java.util.List;

@RestController
@RequestMapping("/api/mahasiswa")
@CrossOrigin
public class MahasiswaController {
	@Autowired
	MahasiswaService service;
	
	@RequestMapping(value="", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<List<MahasiswaDTO>> readAll() {
		List<MahasiswaDTO> mahasiswa = service.getAllMahasiswas();
		return new ResponseEntity<>(mahasiswa, HttpStatus.OK);
	}
	
	@RequestMapping(value="{npm_mhs}", method=RequestMethod.GET, produces="application/json" )
	public ResponseEntity<MahasiswaDTO> read(@PathVariable String npm_mhs) {
		MahasiswaDTO mahasiswa = service.getMahasiswaById(npm_mhs);
		return new ResponseEntity<>(mahasiswa, HttpStatus.OK);
	}
	
	@RequestMapping(value="", method=RequestMethod.POST, produces="application/json" )
	public ResponseEntity<MahasiswaDTO> create(@RequestBody MahasiswaDTO mahasiswa) {
		try {
			service.createMahasiswa(mahasiswa);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="", method=RequestMethod.PUT, produces="application/json" )
	public ResponseEntity<MahasiswaDTO> update(@RequestBody MahasiswaDTO mahasiswa) {
		try {
			service.updateMahasiswa(mahasiswa);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="{npm_mhs}", method=RequestMethod.DELETE, produces="application/json" )
	public ResponseEntity<MahasiswaDTO> delete(@PathVariable String npm_mhs) {
		try {
			service.deleteMahasiswa(npm_mhs);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (WrongParameters p) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InternalServerError u) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
}
	