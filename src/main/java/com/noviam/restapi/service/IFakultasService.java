package com.noviam.restapi.service;

import java.util.List;

import com.noviam.restapi.dto.FakultasDTO;

public interface IFakultasService {
	public List<FakultasDTO> getAllFakultases();
	public FakultasDTO getFakultasById(String id_fakultas);
	public void createFakultas(FakultasDTO fakultas);
	public void updateFakultas(FakultasDTO fakultas);
	public void deleteFakultas(String id_fakultas);

}
