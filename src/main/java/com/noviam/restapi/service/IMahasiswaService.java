package com.noviam.restapi.service;

import java.util.List;

import com.noviam.restapi.dto.MahasiswaDTO;

public interface IMahasiswaService {
	public List<MahasiswaDTO> getAllMahasiswas();
	public MahasiswaDTO getMahasiswaById(String npm_mhs);
	public void createMahasiswa(MahasiswaDTO mahasiswa);
	public void updateMahasiswa(MahasiswaDTO mahasiswa);
	public void deleteMahasiswa(String npm_mhs);

}
