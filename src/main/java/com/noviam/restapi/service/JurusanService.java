package com.noviam.restapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import com.noviam.restapi.dao.IJurusanDAO;
import com.noviam.restapi.dto.JurusanDTOBuilder;
import com.noviam.restapi.dto.JurusanDTO;
import com.noviam.restapi.entities.Jurusan;

@Component
public class JurusanService implements IJurusanService{
	@Autowired
	private IJurusanDAO jurusanDAO;
	
	@Override
	public List<JurusanDTO> getAllJurusans(){
		List<Jurusan> entities = jurusanDAO.getJurusans();
		List<JurusanDTO> jurusans = new ArrayList<JurusanDTO>();
		Iterator<Jurusan> iterator = entities.iterator();
		
		while(iterator.hasNext()) {
			Jurusan jurusan = iterator.next();
			jurusans.add(JurusanDTOBuilder.jurusanToJurusanDTO(jurusan));
		}
		return jurusans;
	}
	
	@Override
	public JurusanDTO getJurusanByID(String id_jurusan) {
		Jurusan jurusan = jurusanDAO.getJurusan(id_jurusan);
		return JurusanDTOBuilder.jurusanToJurusanDTO(jurusan);
	}
	
	@Override
	public void createJurusan(JurusanDTO jurusan) {
		jurusanDAO.createJurusan(JurusanDTOBuilder.jurusanDTOToJurusan(jurusan));
	}
	
	@Override
	public void updateJurusan(JurusanDTO jurusan) {
		jurusanDAO.updateJurusan(JurusanDTOBuilder.jurusanDTOToJurusan(jurusan));
	}
	
	@Override
	public void deleteJurusan(String id_jurusan) {
		jurusanDAO.deleteJurusan(id_jurusan);
	}
	
	@Override
	public List<JurusanDTO> getJurusanDesc(){
		List<Jurusan> entities = jurusanDAO.getJurusanSort();
		List<JurusanDTO> jurusans = new ArrayList<JurusanDTO>();
		Iterator<Jurusan> iterator = entities.iterator();
		
		while(iterator.hasNext()) {
			Jurusan jurusan = iterator.next();
			jurusans.add(JurusanDTOBuilder.jurusanToJurusanDTO(jurusan));
		}
		return jurusans;
	}
	
	
}
