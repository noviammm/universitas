package com.noviam.restapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import com.noviam.restapi.dao.IFakultasDAO;
import com.noviam.restapi.dto.FakultasDTOBuilder;
import com.noviam.restapi.dto.FakultasDTO;
import com.noviam.restapi.entities.Fakultas;

@Component
public class FakultasService implements IFakultasService {
	
	@Autowired
	private IFakultasDAO fakultasDAO;
	
	@Override
	public List<FakultasDTO> getAllFakultases(){
		List<Fakultas> entities = fakultasDAO.getFakultases();
		List<FakultasDTO> fakultases = new ArrayList<FakultasDTO>();
		
		Iterator<Fakultas> iterator = entities.iterator();
		
		while(iterator.hasNext()) {
			Fakultas fakultas = iterator.next();
			fakultases.add(FakultasDTOBuilder.fakultasToFakultasDTO(fakultas));
		}
		return fakultases;
	}
	
	@Override
	public FakultasDTO getFakultasById(String id_fakultas) {
		Fakultas fakultas = fakultasDAO.getFakultas(id_fakultas);	
		return FakultasDTOBuilder.fakultasToFakultasDTO(fakultas);
	}

	@Override
	public void createFakultas(FakultasDTO fakultas) {
		fakultasDAO.createFakultas(FakultasDTOBuilder.fakultasDTOToFakultas(fakultas));
	}

	@Override
	public void updateFakultas(FakultasDTO fakultas) {
		fakultasDAO.updateFakultas(FakultasDTOBuilder.fakultasDTOToFakultas(fakultas));
		
	}

	@Override
	public void deleteFakultas(String id_fakultas) {
		fakultasDAO.deleteFakultas(id_fakultas);
	}
	
	
	
}
