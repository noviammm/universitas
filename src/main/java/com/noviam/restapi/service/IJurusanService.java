package com.noviam.restapi.service;

import java.util.List;
import com.noviam.restapi.dto.JurusanDTO;

public interface IJurusanService {
	public List<JurusanDTO> getAllJurusans();
	public List<JurusanDTO> getJurusanDesc();
	public JurusanDTO getJurusanByID(String id_jurusan);
	public void createJurusan(JurusanDTO jurusan);
	public void updateJurusan(JurusanDTO jurusan);
	public void deleteJurusan(String id_jurusan);
}
