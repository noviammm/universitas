package com.noviam.restapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import com.noviam.restapi.dao.IKrsDAO;
import com.noviam.restapi.dto.KrsDTOBuilder;
import com.noviam.restapi.dto.KrsDTO;
import com.noviam.restapi.entities.Krs;

@Component
public class KrsService implements IKrsService {
	
	@Autowired
	private IKrsDAO krsDAO;
	
	@Override
	public List<KrsDTO> getAllKrses(){
		List<Krs> entities = krsDAO.getKrses();
		List<KrsDTO> krses = new ArrayList<KrsDTO>();
		
		Iterator<Krs> iterator = entities.iterator();
		
		while(iterator.hasNext()) {
			Krs krs = iterator.next();
			krses.add(KrsDTOBuilder.krsToKrsDTO(krs));
		}
		return krses;
	}
	
	@Override
	public List<KrsDTO> getNamaDanMatkul(){
		List<Krs> entities = krsDAO.getNamaDanMatkul();
		List<KrsDTO> krses = new ArrayList<KrsDTO>();
		
		Iterator<Krs> iterator = entities.iterator();
		
		while(iterator.hasNext()) {
			Krs krs = iterator.next();
			krses.add(KrsDTOBuilder.krsToKrsDTO(krs));
		}
		return krses;
	}
	
	@Override
	public KrsDTO getKrsById(String id_krs) {
		Krs krs = krsDAO.getKrs(id_krs);	
		return KrsDTOBuilder.krsToKrsDTO(krs);
	}

	@Override
	public void createKrs(KrsDTO krs) {
		krsDAO.createKrs(KrsDTOBuilder.krsDTOToKrs(krs));
	}

	@Override
	public void updateKrs(KrsDTO krs) {
		krsDAO.updateKrs(KrsDTOBuilder.krsDTOToKrs(krs));
		
	}

	@Override
	public void deleteKrs(String id_krs) {
		krsDAO.deleteKrs(id_krs);
	}	
}
