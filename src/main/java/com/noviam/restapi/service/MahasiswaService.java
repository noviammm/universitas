package com.noviam.restapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import com.noviam.restapi.dao.IMahasiswaDAO;
import com.noviam.restapi.dto.MahasiswaDTOBuilder;
import com.noviam.restapi.dto.MahasiswaDTO;
import com.noviam.restapi.entities.Mahasiswa;

@Component
public class MahasiswaService implements IMahasiswaService {
	
	@Autowired
	private IMahasiswaDAO mahasiswaDAO;
	
	@Override
	public List<MahasiswaDTO> getAllMahasiswas(){
		List<Mahasiswa> entities = mahasiswaDAO.getMahasiswas();
		List<MahasiswaDTO> mahasiswas = new ArrayList<MahasiswaDTO>();
		
		Iterator<Mahasiswa> iterator = entities.iterator();
		
		while(iterator.hasNext()) {
			Mahasiswa mahasiswa = iterator.next();
			mahasiswas.add(MahasiswaDTOBuilder.mahasiswaToMahasiswaDTO(mahasiswa));
		}
		return mahasiswas;
	}
	
	@Override
	public MahasiswaDTO getMahasiswaById(String npm_mhs) {
		Mahasiswa mahasiswa = mahasiswaDAO.getMahasiswa(npm_mhs);	
		return MahasiswaDTOBuilder.mahasiswaToMahasiswaDTO(mahasiswa);
	}

	@Override
	public void createMahasiswa(MahasiswaDTO mahasiswa) {
		mahasiswaDAO.createMahasiswa(MahasiswaDTOBuilder.mahasiswaDTOToMahasiswa(mahasiswa));
	}

	@Override
	public void updateMahasiswa(MahasiswaDTO mahasiswa) {
		mahasiswaDAO.updateMahasiswa(MahasiswaDTOBuilder.mahasiswaDTOToMahasiswa(mahasiswa));
		
	}

	@Override
	public void deleteMahasiswa(String npm_mhs) {
		mahasiswaDAO.deleteMahasiswa(npm_mhs);
	}
	
	
	
}
