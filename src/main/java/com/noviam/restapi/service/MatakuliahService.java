package com.noviam.restapi.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.noviam.restapi.dao.IMatakuliahDAO;
import com.noviam.restapi.dto.MatakuliahDTO;
import com.noviam.restapi.dto.MatakuliahDTOBuilder;
import com.noviam.restapi.entities.Matakuliah;

@Component
public class MatakuliahService implements IMatakuliahService{

	@Autowired
	private IMatakuliahDAO matakuliahDAO;
	
	@Override
	public List<MatakuliahDTO> getAllMatkuls() {
		List<Matakuliah> entities = matakuliahDAO.getMatkuls();
		List<MatakuliahDTO> matkuls = new ArrayList<MatakuliahDTO>();
		
		Iterator<Matakuliah> iterator = entities.iterator();
		
		while(iterator.hasNext()) {
			Matakuliah matkul = iterator.next();
			matkuls.add(MatakuliahDTOBuilder.matakuliahToMatakuliahDTO(matkul));
		}
		return matkuls;
	}

	@Override
	public MatakuliahDTO getMatkulById(String id_matkul) {
		Matakuliah matkul = matakuliahDAO.getMatkul(id_matkul);
		return MatakuliahDTOBuilder.matakuliahToMatakuliahDTO(matkul);
	}

	@Override
	public void createMatkul(MatakuliahDTO matkul) {
		matakuliahDAO.createMatkul(MatakuliahDTOBuilder.matakuliahDTOToMatakuliah(matkul));
	}

	@Override
	public void updateMatkul(MatakuliahDTO matkul) {
		matakuliahDAO.updateMatkul(MatakuliahDTOBuilder.matakuliahDTOToMatakuliah(matkul));		
	}

	@Override
	public void deleteMatkul(String id_matkul) {
		matakuliahDAO.deleteMatkul(id_matkul);
		
	}

}
