package com.noviam.restapi.service;

import java.util.List;

import com.noviam.restapi.dto.MatakuliahDTO;

public interface IMatakuliahService {
	public List<MatakuliahDTO> getAllMatkuls();
	public MatakuliahDTO getMatkulById(String id_matkul);
	public void createMatkul(MatakuliahDTO matkul);
	public void updateMatkul(MatakuliahDTO matkul);
	public void deleteMatkul(String id_matkul);
	
}
