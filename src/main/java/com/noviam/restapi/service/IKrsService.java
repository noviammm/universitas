package com.noviam.restapi.service;

import java.util.List;

import com.noviam.restapi.dto.KrsDTO;

public interface IKrsService {
	public List<KrsDTO> getAllKrses();
	public List<KrsDTO> getNamaDanMatkul();
	public KrsDTO getKrsById(String id_krs);
	public void createKrs(KrsDTO krs);
	public void updateKrs(KrsDTO krs);
	public void deleteKrs(String id_krs);

}
