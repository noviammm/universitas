package com.noviam.restapi.dto;

import com.noviam.restapi.entities.Fakultas;

public class FakultasDTOBuilder {
	public static FakultasDTO fakultasToFakultasDTO(Fakultas fakultas) {
		return new FakultasDTO(fakultas.getId_fakultas(), fakultas.getNama_fakultas());	
	}
	
	public static Fakultas fakultasDTOToFakultas(FakultasDTO fakultas) {
		return new Fakultas(fakultas.getId_fakultas(), fakultas.getNama_fakultas());
	}
	

}
