package com.noviam.restapi.dto;

import com.noviam.restapi.entities.Matakuliah;

public class MatakuliahDTOBuilder {
	public static MatakuliahDTO matakuliahToMatakuliahDTO(Matakuliah matkul) {
		return new MatakuliahDTO(matkul.getId_matkul(), matkul.getNama_matkul(), matkul.getSks_matkul());
	}
	
	public static Matakuliah matakuliahDTOToMatakuliah(MatakuliahDTO matkul) {
		return new Matakuliah(matkul.getId_matkul(), matkul.getNama_matkul(), matkul.getSks_matkul());
	}

}
