package com.noviam.restapi.dto;

import com.noviam.restapi.entities.Jurusan;

public class JurusanDTOBuilder {
	public static JurusanDTO jurusanToJurusanDTO(Jurusan jurusan) {
		return new JurusanDTO(
				jurusan.getIdJurusan(),
				jurusan.getIdFakultas(),
				jurusan.getNamaJurusan()
		);
	}
	
	public static Jurusan jurusanDTOToJurusan(JurusanDTO jurusan) {
			return new Jurusan(
					jurusan.getId_jurusan(),
					jurusan.getId_fakultas(),
					jurusan.getNama_jurusan()
			);
	}
}
