package com.noviam.restapi.dto;

public class JurusanDTO {
	private String id_jurusan;
	private String id_fakultas;
	private String nama_jurusan;
	
	public JurusanDTO(
			String id_jurusan,
			String id_fakultas,
			String nama_jurusan
	) {
		super();
		this.id_jurusan=id_jurusan;
		this.id_fakultas=id_fakultas;
		this.nama_jurusan=nama_jurusan;
	}

	public String getId_jurusan() {
		return id_jurusan;
	}

	public void setId_jurusan(String id_jurusan) {
		this.id_jurusan = id_jurusan;
	}

	public String getId_fakultas() {
		return id_fakultas;
	}

	public void setId_fakultas(String id_fakultas) {
		this.id_fakultas = id_fakultas;
	}

	public String getNama_jurusan() {
		return nama_jurusan;
	}

	public void setNama_jurusan(String nama_jurusan) {
		this.nama_jurusan = nama_jurusan;
	}
	
	
}
