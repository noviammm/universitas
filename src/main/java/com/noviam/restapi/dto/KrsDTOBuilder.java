package com.noviam.restapi.dto;

import com.noviam.restapi.entities.Krs;

public class KrsDTOBuilder {
	public static KrsDTO krsToKrsDTO(Krs krs) {
		return new KrsDTO(krs.getNo(), krs.getId_krs(), krs.getNpm_mhs(),
				krs.getId_jurusan(), krs.getId_matkul(), krs.getDospem(),
				krs.getSemester(), krs.getTahun_ajaran());	
	}
	
	public static Krs krsDTOToKrs(KrsDTO krs) {
		return new Krs(krs.getNo(), krs.getId_krs(), krs.getNpm_mhs(),
				krs.getId_jurusan(), krs.getId_matkul(), krs.getDospem(),
				krs.getSemester(), krs.getTahun_ajaran());
	}
	

}
