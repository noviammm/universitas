package com.noviam.restapi.dto;

import java.util.Date;

public class MahasiswaDTO {
	private String npm_mhs;
	private String nama_mhs;
	private String tempatlahir_mhs;
	private Date tgllahir_mhs;
	private String alamat_mhs;
	private String ayah_mhs;
	private String ibu_mhs;
	private String id_jurusan;
	
	public MahasiswaDTO(String npm_mhs, String nama_mhs, String tempatlahir_mhs,
			Date tgllahir_mhs, String alamat_mhs, String ayah_mhs, String ibu_mhs,
			String id_jurusan) {
	super();
	this.npm_mhs = npm_mhs;
	this.nama_mhs = nama_mhs;
	this.tempatlahir_mhs = tempatlahir_mhs;
	this.tgllahir_mhs = tgllahir_mhs;
	this.alamat_mhs = alamat_mhs;
	this.ayah_mhs = ayah_mhs;
	this.ibu_mhs = ibu_mhs;
	this.id_jurusan = id_jurusan;
	}

	public String getNpm_mhs() {
		return npm_mhs;
	}

	public void setNpm_mhs(String npm_mhs) {
		this.npm_mhs = npm_mhs;
	}

	public String getNama_mhs() {
		return nama_mhs;
	}

	public void setNama_mhs(String nama_mhs) {
		this.nama_mhs = nama_mhs;
	}

	public String getTempatlahir_mhs() {
		return tempatlahir_mhs;
	}

	public void setTempatlahir_mhs(String tempatlahir_mhs) {
		this.tempatlahir_mhs = tempatlahir_mhs;
	}

	public Date getTgllahir_mhs() {
		return tgllahir_mhs;
	}

	public void setTgllahir_mhs(Date tgllahir_mhs) {
		this.tgllahir_mhs = tgllahir_mhs;
	}

	public String getAlamat_mhs() {
		return alamat_mhs;
	}

	public void setAlamat_mhs(String alamat_mhs) {
		this.alamat_mhs = alamat_mhs;
	}

	public String getAyah_mhs() {
		return ayah_mhs;
	}

	public void setAyah_mhs(String ayah_mhs) {
		this.ayah_mhs = ayah_mhs;
	}

	public String getIbu_mhs() {
		return ibu_mhs;
	}

	public void setIbu_mhs(String ibu_mhs) {
		this.ibu_mhs = ibu_mhs;
	}

	public String getId_jurusan() {
		return id_jurusan;
	}

	public void setId_jurusan(String id_jurusan) {
		this.id_jurusan = id_jurusan;
	}
	
	
	

}
