package com.noviam.restapi.dto;

public class FakultasDTO {
	private String id_fakultas;
	private String nama_fakultas;
	
	public FakultasDTO(String id_fakultas, String nama_fakultas) {
	super();
	this.id_fakultas = id_fakultas;
	this.nama_fakultas = nama_fakultas;
	}

	public String getId_fakultas() {
		return id_fakultas;
	}

	public void setId_fakultas(String id_fakultas) {
		this.id_fakultas = id_fakultas;
	}

	public String getNama_fakultas() {
		return nama_fakultas;
	}

	public void setNama_fakultas(String nama_fakultas) {
		this.nama_fakultas = nama_fakultas;
	}
	
	

}
