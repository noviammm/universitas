package com.noviam.restapi.dto;

import com.noviam.restapi.entities.Mahasiswa;

public class MahasiswaDTOBuilder {
	public static MahasiswaDTO mahasiswaToMahasiswaDTO(Mahasiswa mahasiswa) {
		return new MahasiswaDTO(mahasiswa.getNpm_mhs(), mahasiswa.getNama_mhs(), mahasiswa.getTempatlahir_mhs(),
				mahasiswa.getTgllahir_mhs(), mahasiswa.getAlamat_mhs(), mahasiswa.getAyah_mhs(), mahasiswa.getIbu_mhs(),
				mahasiswa.getId_jurusan());	
	}
	
	public static Mahasiswa mahasiswaDTOToMahasiswa(MahasiswaDTO mahasiswa) {
		return new Mahasiswa(mahasiswa.getNpm_mhs(), mahasiswa.getNama_mhs(), mahasiswa.getTempatlahir_mhs(),
				mahasiswa.getTgllahir_mhs(), mahasiswa.getAlamat_mhs(), mahasiswa.getAyah_mhs(), mahasiswa.getIbu_mhs(),
				mahasiswa.getId_jurusan());
	}
	
}
